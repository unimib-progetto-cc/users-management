package it.unimib.cc.dealer.users.repositories;

import it.unimib.cc.dealer.users.entities.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<ApplicationUser, Long>, JpaSpecificationExecutor<ApplicationUser> {
    List<ApplicationUser> findByEmailContainingIgnoreCaseAndNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(String email, String name, String surname);

    Optional<ApplicationUser> findByEmail(String email);

    Boolean existsByEmail(String email);
}
