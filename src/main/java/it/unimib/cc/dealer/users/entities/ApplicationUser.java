package it.unimib.cc.dealer.users.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.hateoas.RepresentationModel;

@Getter
@Setter
@Entity
@JacksonXmlRootElement(localName = "user")
public class ApplicationUser extends RepresentationModel<ApplicationUser> {

	@Id
    @GeneratedValue
    private Long id;

    private String name;

    private String surname;

    @Column(unique = true)
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
}
