package it.unimib.cc.dealer.users.payloads.requests;

import lombok.Getter;

import javax.validation.constraints.Email;

@Getter
public class UpdateRequest {
    public String name;

    public String surname;

    @Email
    public String email;

    public String currentPassword;

    public String newPassword;

    public String newPasswordConfirmation;
}
