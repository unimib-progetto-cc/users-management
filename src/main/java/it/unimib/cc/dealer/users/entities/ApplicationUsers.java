package it.unimib.cc.dealer.users.entities;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "users")
public class ApplicationUsers extends RepresentationModel<ApplicationUsers> {

	@JacksonXmlProperty(localName = "user")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ApplicationUser> users = new ArrayList<>();

	public ApplicationUsers(List<ApplicationUser> users) {
		super();
		this.users = users;
	}

	public List<ApplicationUser> getUsers() {
		return users;
	}

	public void setUsers(List<ApplicationUser> users) {
		this.users = users;
	}
}
