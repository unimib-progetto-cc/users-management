package it.unimib.cc.dealer.users.security.services;

import it.unimib.cc.dealer.users.entities.ApplicationUser;
import it.unimib.cc.dealer.users.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser user = this.userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        return UserDetailsImpl.build(user);
    }
}
