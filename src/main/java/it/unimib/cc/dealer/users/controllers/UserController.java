package it.unimib.cc.dealer.users.controllers;

import it.unimib.cc.dealer.users.entities.ApplicationUser;
import it.unimib.cc.dealer.users.entities.ApplicationUsers;
import it.unimib.cc.dealer.users.payloads.requests.UpdateRequest;
import it.unimib.cc.dealer.users.services.UserService;
import it.unimib.cc.dealer.users.utils.SecurityUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.ArrayList;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> list(HttpServletRequest request,
			@RequestParam(required = false, defaultValue = "") String email,
			@RequestParam(required = false, defaultValue = "") String name,
			@RequestParam(required = false, defaultValue = "") String surname) {
		SecurityUtils.checkParameters(request, Arrays.asList(new String[] { "email", "name", "surname" }));
		
		ApplicationUsers users = this.userService.search(email, name, surname);

		users.getUsers().forEach((booking) -> {
			booking.add(WebMvcLinkBuilder.linkTo(UserController.class).slash(booking.getId()).withSelfRel());
		});

		users.add(WebMvcLinkBuilder.linkTo(UserController.class).withSelfRel());

		return ResponseEntity.ok(users);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> store(HttpServletRequest request, @Valid @RequestBody ApplicationUser user) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		ApplicationUser newUser = this.userService.save(user);
		newUser.add(WebMvcLinkBuilder.linkTo(UserController.class).slash(newUser.getId()).withSelfRel());

		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> show(HttpServletRequest request, @PathVariable("id") Long id) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		ApplicationUser user = this.userService.findById(id);
		user.add(WebMvcLinkBuilder.linkTo(UserController.class).slash(user.getId()).withSelfRel());

		return ResponseEntity.ok(user);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> update(HttpServletRequest request, @PathVariable Long id,
			@Valid @RequestBody UpdateRequest updateRequest) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		this.userService.update(id, updateRequest);

		return ResponseEntity.noContent().build();
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> delete(HttpServletRequest request, @PathVariable Long id) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		ApplicationUser user = this.userService.delete(id);

		return ResponseEntity.ok(user);
	}
}
