package it.unimib.cc.dealer.users.controllers;

import it.unimib.cc.dealer.users.entities.ApplicationUser;
import it.unimib.cc.dealer.users.payloads.requests.RegisterRequest;
import it.unimib.cc.dealer.users.services.UserService;
import it.unimib.cc.dealer.users.utils.SecurityUtils;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {
	@Autowired
	private UserService userService;

	@PostMapping(value = "/register", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> register(HttpServletRequest request, @RequestBody RegisterRequest registerRequest) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		ApplicationUser user = this.userService.register(registerRequest);
		user.add(WebMvcLinkBuilder.linkTo(UserController.class).slash(user.getId()).withSelfRel());

		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@GetMapping(value = "/profile", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> profile(HttpServletRequest request, Authentication authentication) {
		SecurityUtils.checkParameters(request, new ArrayList<>());

		UserDetails principal = (UserDetails) authentication.getPrincipal();

		ApplicationUser user = this.userService.findByEmail(principal.getUsername());
		user.add(WebMvcLinkBuilder.linkTo(UserController.class).slash(user.getId()).withSelfRel());

		return ResponseEntity.ok(user);
	}
}
