package it.unimib.cc.dealer.users.services;

import it.unimib.cc.dealer.users.entities.ApplicationUser;
import it.unimib.cc.dealer.users.entities.ApplicationUsers;
import it.unimib.cc.dealer.users.exceptions.BadRequestException;
import it.unimib.cc.dealer.users.exceptions.ResourceNotFoundException;
import it.unimib.cc.dealer.users.payloads.requests.RegisterRequest;
import it.unimib.cc.dealer.users.payloads.requests.UpdateRequest;
import it.unimib.cc.dealer.users.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public ApplicationUsers findAll() {
		return new ApplicationUsers(this.userRepository.findAll());
	}

	public ApplicationUsers search(String email, String name, String surname) {
		return new ApplicationUsers(this.userRepository
				.findByEmailContainingIgnoreCaseAndNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(email, name, surname));
	}

	public ApplicationUser save(ApplicationUser user) {
		user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));

		return this.userRepository.save(user);
	}

	public ApplicationUser register(RegisterRequest registerRequest) throws BadRequestException {
		if (userRepository.existsByEmail(registerRequest.getEmail())) {
			throw new BadRequestException("Email address is already in use!");
		}

		this.assertPasswordHasCorrectConfirmation(registerRequest.getPassword(),
				registerRequest.getPasswordConfirmation());

		ApplicationUser user = new ApplicationUser();
		user.setEmail(registerRequest.getEmail());
		user.setName(registerRequest.getName());
		user.setSurname(registerRequest.getSurname());
		user.setPassword(this.bCryptPasswordEncoder.encode(registerRequest.getPassword()));

		return this.userRepository.save(user);
	}

	public ApplicationUser findById(Long id) {
		return this.userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found with id " + id));
	}

	public ApplicationUser findByEmail(String email) {
		return this.userRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFoundException("User not found with email " + email));
	}

	public ApplicationUser update(Long id, UpdateRequest updateRequest) {
		ApplicationUser user = this.findById(id);

		if (updateRequest.getNewPassword() != null && updateRequest.getNewPassword().length() > 0) {
			if (!this.bCryptPasswordEncoder.matches(updateRequest.getCurrentPassword(), user.getPassword())) {
				throw new BadRequestException("The provided current password does not match the stored one!");
			}

			this.assertPasswordHasCorrectConfirmation(updateRequest.getNewPassword(),
					updateRequest.getNewPasswordConfirmation());

			user.setPassword(this.bCryptPasswordEncoder.encode(updateRequest.getNewPassword()));
		}

		if (updateRequest.getName() != null && updateRequest.getName().length() > 0) {
			user.setName(updateRequest.getName());
		}
		if (updateRequest.getSurname() != null && updateRequest.getSurname().length() > 0) {
			user.setSurname(updateRequest.getSurname());	
		}
		if (updateRequest.getEmail() != null && updateRequest.getEmail().length() > 0) {
			user.setEmail(updateRequest.getEmail());
		}

		return this.userRepository.save(user);
	}

	public ApplicationUser delete(Long id) {
		ApplicationUser user = this.findById(id);

		this.userRepository.deleteById(id);

		return user;
	}

	protected void assertPasswordHasCorrectConfirmation(String password, String passwordConfirmation) {
		if (!password.equals(passwordConfirmation)) {
			throw new BadRequestException("The provided password and its confirmation do not match!");
		}
	}
}
