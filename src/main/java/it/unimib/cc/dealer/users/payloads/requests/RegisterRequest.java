package it.unimib.cc.dealer.users.payloads.requests;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
public class RegisterRequest {
    @NotBlank
    public String name;

    @NotBlank
    public String surname;

    @NotBlank
    @Email
    public String email;

    @NotBlank
    public String password;

    public String passwordConfirmation;
}
