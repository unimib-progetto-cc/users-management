package it.unimib.cc.dealer.users.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import it.unimib.cc.dealer.users.exceptions.BadRequestException;

public class SecurityUtils {

	public static final void checkParameters(HttpServletRequest request, List<String> allowedParameters) {
		ArrayList<String> actualParameters = Collections.list(request.getParameterNames());

		if (actualParameters.size() > allowedParameters.size()) {
			actualParameters.removeAll(allowedParameters);

			if (!actualParameters.isEmpty()) {
				throw new BadRequestException("Parameters not allowed: " + actualParameters);
			}
		}
	}
}