# Build phase.
FROM maven:3.5-jdk-8 as build
COPY ./ /user-management
WORKDIR /user-management
RUN mvn clean package -Dmaven.test.skip=true

# Run phase.
FROM openjdk:8-jre-alpine
ARG VERSION
COPY --from=build /user-management/target/users-$VERSION-SNAPSHOT.jar /app/users-app.jar
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["java", "-jar", "users-app.jar"]
